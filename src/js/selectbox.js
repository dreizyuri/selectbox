;(function($) {
  'use strict';
  
  $.fn.selectBox = function( options ) {
    
    var settings = $.extend({
        afterSelect: function (val, text) {}
    }, options);

    return this.each(function() {
        var _this = $(this), _options, _defaultSelect, _items;

        var init = function() {
          _this.wrapAll($('<div />').addClass('selectbox'));
          _options = getOptions();
          _defaultSelect = createDefault();
          _this.after(_defaultSelect);
          _defaultSelect.after(createOptions());
        };

        // Select options
        var getOptions = function() {
          return _this.find('option');
        };

        // Create default select
        var createDefault = function() {
          return $('<div />')
            .addClass('selectbox--default')
            .text(_options.first().text().trim());
        };

        // Create options
        var createOptions = function() {
          _items = $('<ul />').addClass('selectbox--items');
          _options.each(function() {
            _items.append($('<li />')
              .attr('data-value', $(this).val())
              .text($(this).text()).addClass('selectbox--item'));
          });
          return _items;
        }
        init();
        
        $('body').on('click', '.selectbox--item', function() {
          var $this = $(this), $selectbox = $('.selectbox');
          $selectbox.find('select').val($this.data('value'));
          $selectbox.find('.selectbox--default').text($this.text());
          $selectbox.removeClass('active');
          settings.afterSelect($this.data('value'), $this.text());
        }).on('click', '.selectbox--default', function() {
          $(this).parent().toggleClass('active')
        });

        $(document).mousemove(function (e) {
            var $this = $('.selectbox');
            if (!$this.is(e.target) && $this.has(e.target).length === 0)
                $this.removeClass('active');
        });

    });
  };

})(jQuery);